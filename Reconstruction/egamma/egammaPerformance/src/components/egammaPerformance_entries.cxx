#include "egammaPerformance/photonMonTool.h"
#include "egammaPerformance/electronMonTool.h"
#include "egammaPerformance/forwardElectronMonTool.h"
#include "egammaPerformance/ZeeTaPMonTool.h"

DECLARE_COMPONENT( photonMonTool )
DECLARE_COMPONENT( electronMonTool )
DECLARE_COMPONENT( forwardElectronMonTool )
DECLARE_COMPONENT( ZeeTaPMonTool )

